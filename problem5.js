function lessTwoK(yearOfCars){
    let lessTwoK = [];
    if(!Array.isArray(yearOfCars) || yearOfCars.length === 0 || yearOfCars === undefined)
    {
        const arr = [];
        return arr;
    }

    for(let i =0; i < yearOfCars.length; i++)
    {
        if(yearOfCars[i] < 2000)
        {
            lessTwoK.push(yearOfCars[i]);
        }
    }

    return lessTwoK;
}

module.exports = lessTwoK;
