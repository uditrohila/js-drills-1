const problem1 = require("../problem1.js");
const inventory = require("../cars");

// multiple test case check
// const result1 = problem1([]);
// console.log(result1);
// test case when no search id is passed
// const result1 = problem1(inventory);
// console.log(result1);


const result = problem1(inventory,33);
let output = `car is a ${result.car_year} ${result.car_make} ${result.car_model}`
console.log(output);
